/*
 * Config.h
 *
 *  Created on: 2015/04/14
 *      Author: Toida
 */

#ifndef CONFIG_H_
#define CONFIG_H_

/* BGM */
//#define BGM_TITLE		"bgm/TitleBGM2.mp3"
//#define BGM_GAMEMAIN	"bgm/MainBGM3.mp3"
//#define BGM_GAMEOVER	"bgm/GameOverBGM.mp3"

/* SE */
//#define SE_SWING		"sound/shoot1.ogg"
//#define SE_HIT			"sound/shoot1.ogg"
//#define SE_GUARD		"sound/shoot1.ogg"

/* Particle */
//#define OPENING_PARTICLE        "particles/openingParticle.plist"

/* Particle Less Ver. */
//#define OPENING_PARTICLE_LESS            40;

/* Font */
#define MAIN_FONT	"lsansd.ttf"
#define CREDIT_FONT	"lsansd.ttf"

/* マルチレゾリューション対応 */
#define BASE_WIDTH			800
#define BASE_HEIGHT			1280
#define FONT_SIZE(__SIZE)(cocos2d::CCEGLView::sharedOpenGLView()->getDesignResolutionSize().width / BASE_WIDTH * __SIZE)
#define WINSIZE CCDirector::sharedDirector()->getWinSize()
#define WIN_SCALE CCDirector::sharedDirector()->getWinSize().width / BASE_WIDTH

/* スプラッシュ画像 */
//#define SPALSH_SCREEN		"Default-Portrait.png"

/* Image */
//#define CMOJI_C2_IPAD_IMAGE	"images/c2-ipad.png"

// Icon
//#define GAME_START_ICON		"images/icon/game-ipad.png"
//#define GAME_OPTION_ICON	"images/icon/option-ipad.png"
//#define GAME_CREDIT_ICON	"images/icon/credit-ipad.png"
//#define GAME_RANKING_ICON   "images/icon/ranking-ipadhd.png"
//#define GAME_BACk_ICON		"images/icon/back-ipad.png"

#endif /* CONFIG_H_ */
