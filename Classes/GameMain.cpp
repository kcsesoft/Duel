/*
 * GameMain.cpp
 *
 *  Created on: 2015/04/14
 *      Author: Toida
 */

/******************************************************************************
 include
******************************************************************************/
#include "cocos2d.h"
//#include "SimpleAudioEngine.h"
//#include "Title.h"
#include "GameMain.h"
//#include "Option.h"
#include "Config.h"

USING_NS_CC;

CCScene* GameMain::scene()
{
	// 'scene' is an autorelease object
	CCScene *scene = CCScene::create();

	// 'layer' is an autorelease object
	GameMain *Gamelayer = GameMain::create();

	// add layer as a child to scene
	scene->addChild(Gamelayer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameMain::init()
{
	if ( !CCLayer::init() )
	{
		return false;
	}

	// 画面サイズ取得
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

//	// 背景パーティクル表示
//	CCParticleSystemQuad *backgroundParticle = CCParticleSystemQuad::create(GALAXY_PARTICLE);
//	backgroundParticle->setPosition(ccp(winSize.width / 2, winSize.height / 2));
//	backgroundParticle->setAutoRemoveOnFinish(true);
//	backgroundParticle->setScale(2); // 暫定対応　機種毎の調整必要
//	this->addChild(backgroundParticle);

	/* シングルタッチON */
	this->setTouchMode(kCCTouchesOneByOne);
	this->setTouchEnabled(true);

	/* バックキー、ホームキーを検出可能にする */
	this->setKeypadEnabled(true);

	/* 文字列関連 */
	m_label_Ready = CCLabelTTF::create("Ready", MAIN_FONT, 80 * WIN_SCALE);
	m_label_Ready->setColor(ccc3(255,240,0));
	m_label_Ready->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	this->addChild(m_label_Ready, 1);
	m_label_Ready->setVisible(false);

	m_label_Go = CCLabelTTF::create("Go", MAIN_FONT, 80 * WIN_SCALE);
	m_label_Go->setColor(ccc3(255,240,0));
	m_label_Go->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	this->addChild(m_label_Go, 1);
	m_label_Go->setVisible(false);

	m_label_YouWin = CCLabelTTF::create("You Win", MAIN_FONT, 80 * WIN_SCALE);
	m_label_YouWin->setColor(ccc3(255,240,0));
	m_label_YouWin->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	this->addChild(m_label_YouWin, 1);
	m_label_YouWin->setVisible(false);

	m_label_YouLose = CCLabelTTF::create("You Lose", MAIN_FONT, 80 * WIN_SCALE);
	m_label_YouLose->setColor(ccc3(255,240,0));
	m_label_YouLose->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	this->addChild(m_label_YouLose, 1);
	m_label_YouLose->setVisible(false);

	/* 画像関連 */

    return true;
}

/* cocos2dxタッチ開始関数 */
bool GameMain::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	/* タップポイント取得 */
	CCPoint location = this->convertTouchToNodeSpace(pTouch);

	return true;
}

/* cocos2dxタッチ移動関数 */
void GameMain::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
	/* タップポイント取得 */
	CCPoint location = this->convertTouchToNodeSpace(pTouch);
}

/* cocos2dxタッチ終了関数 */
void GameMain::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
	/* タップポイント取得 */
	CCPoint location = this->convertTouchToNodeSpace(pTouch);
}

/* バックキー押下時処理 */
void GameMain::keyBackClicked()
{
	CCLOG("GameMain::keyBackClicked ================================");
	CCDirector::sharedDirector()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	/* iOS */
	exit(0);
#endif
}

/* ホームキー押下時処理 */
void GameMain::keyMenuClicked()
{
	CCLOG("GameMain::keyMenuClicked ================================");
}
